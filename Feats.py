import sys
import numpy as np


# Iterable Features class
class Feats(object):
    def __init__(self, filename=None):
        if filename is None:
            self.input = sys.stdin
        else:
            self.input = open(filename, 'r')

    def get_utt(self):
        for line in self.input:
            if '[' in line:
                return line
        return False

    def __iter__(self):
        return self

    def next(self):
        feat_matrix = []
        for line in self.input:
            lnsplit = line.strip().split()
            if '[' in line:
                key = lnsplit[0]
            elif ']' in line:
                lnsplit = lnsplit[:-1]
                feat_matrix.append(lnsplit)
                return key, np.array(feat_matrix, dtype=np.float)
            else:
                feat_matrix.append(lnsplit)
        raise StopIteration

    def __next__(self):
        feat_matrix = []
        for line in self.input:
            lnsplit = line.strip().split()
            if '[' in line:
                key = lnsplit[0]
            elif ']' in line:
                lnsplit = lnsplit[:-1]
                feat_matrix.append(lnsplit)
                return key, np.array(feat_matrix, dtype=np.float)
            else:
                feat_matrix.append(lnsplit)
        raise StopIteration
