#!/usr/bin/env python
import numpy as np
import optparse
import tensorflow as tf
import re
from six.moves import xrange
from Feats import Feats
from random import shuffle

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-t", "--train_labels", dest="train_labels", default="", type="string", help="Training labels file")
optparser.add_option("-l", "--length_file", dest="length_file", default="", type="string", help="Number of frames per file")
optparser.add_option("-m", "--mode", dest="mode", default="t", type="string", help="Run on train model model (t), or predict changes mode (p)")
opts = optparser.parse_args()[0]

## Code based on the Tensorflow MNIST example available at:
# https://www.tensorflow.org/versions/r0.10/tutorials/mnist/pros/index.html
# Global constants describing the data set.
WINDOW_SIZE = 50
NUM_FEATURES = 43
NUM_CLASSES = 5
BATCH_SIZE = 50
CHECK_POINT_DIR = "/home/andus/Experiment1"

INITIAL_LEARNING_RATE = 1e-4  # Initial learning rate.

TOWER_NAME = "TOWER"
def _activation_summary(var):
    tensor_name = re.sub('%s_[0-9]*/' % TOWER_NAME, '', var.op.name)
    """Attach a lot of summaries to a Tensor."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + tensor_name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_sum(tf.square(var - mean)))
        tf.scalar_summary('sttdev/' + tensor_name, stddev)
        tf.scalar_summary('max/' + tensor_name, tf.reduce_max(var))
        tf.scalar_summary('min/' + tensor_name, tf.reduce_min(var))
        tf.histogram_summary(tensor_name, var)

def _variable_on_gpu(name, shape, initializer):
    with tf.device('/gpu:0'):
        dtype = tf.float32
        var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)
    return var


def _variable_with_weight_decay(name, shape, stddev, wd):
    dtype = tf.float32
    var = _variable_on_gpu(
        name,
        shape,
        tf.truncated_normal_initializer(stddev=stddev, dtype=dtype))
    if wd is not None:
        weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var


def inference(features):
    # hidden layer 1
    with tf.variable_scope('hidden1') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        weights = _variable_with_weight_decay('weights', shape=[WINDOW_SIZE * NUM_FEATURES, 1024],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [1024], tf.constant_initializer(1))
        hidden1 = tf.nn.relu(tf.matmul(features, weights) + biases, name=scope.name)
        # _activation_summary(hidden1)

    # hidden layer 2
    with tf.variable_scope('hidden2') as scope:
        weights = _variable_with_weight_decay('weights', shape=[1024, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(1))
        hidden2 = tf.nn.relu(tf.matmul(hidden1, weights) + biases, name=scope.name)
        # _activation_summary(hidden2)

    # hidden layer 3
    with tf.variable_scope('hidden3') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(1))
        hidden3 = tf.nn.relu(tf.matmul(hidden2, weights) + biases, name=scope.name)
        # _activation_summary(hidden3)

    # hidden layer 4
    with tf.variable_scope('hidden4') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(1))
        hidden4 = tf.nn.relu(tf.matmul(hidden3, weights) + biases, name=scope.name)
        # _activation_summary(hidden4)

    # hidden layer 5
    with tf.variable_scope('hidden5') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 256],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [256], tf.constant_initializer(1))
        hidden5 = tf.nn.relu(tf.matmul(hidden4, weights) + biases, name=scope.name)
        # _activation_summary(hidden5)

    # softmax, i.e. softmax(WX + b)
    with tf.variable_scope('softmax_linear') as scope:
        weights = _variable_with_weight_decay('weights', [256, NUM_CLASSES],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [NUM_CLASSES],
                                  tf.constant_initializer(1))
        softmax_linear = tf.add(tf.matmul(hidden5, weights), biases, name=scope.name)
        # _activation_summary(softmax_linear)

    return softmax_linear


def loss(logits, labels):
    """Calculates the loss from the logits and the labels.

    Args:
      logits: Logits tensor, float - [batch_size, NUM_CLASSES].
      labels: Labels tensor, int32 - [batch_size].

    Returns:
      loss: Loss tensor of type float.
    """
    # labels = tf.to_int64(labels)
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, labels, name='xentropy')
    # Create a summary to monitor the cost function
    _activation_summary(cross_entropy)
    loss_op = tf.reduce_mean(cross_entropy, name='xentropy_mean')
    _activation_summary(loss_op)
    return loss_op


def training(loss_op, learning_rate):
    """Sets up the training Ops.
    Creates an optimizer and applies the gradients to all trainable variables.
    The Op returned by this function is what must be passed to the
    `sess.run()` call to cause the model to train.

    Args:
      loss_op: Loss tensor, from loss().
      learning_rate: The learning rate to use for gradient descent.

    Returns:
      train_op: The Op for training.
    """
    # Create the gradient descent optimizer with the given learning rate.
    optimizer = tf.train.AdamOptimizer(learning_rate)
    # Create a variable to track the global step.
    global_step = tf.Variable(0, name='global_step', trainable=False)
    # Use the optimizer to apply the gradients that minimize the loss
    # (and also increment the global step counter) as a single training step.
    train_op = optimizer.minimize(loss_op, global_step=global_step)
    return train_op

def regions(condition):
    d = np.diff(condition)
    idx, = d.nonzero()
    idx += 1
    if condition[0]:
        idx = np.r_[0, idx]
    if condition[-1]:
        idx = np.r_[idx, condition.size]  # Edit
    idx.shape = (-1, 2)
    return idx

feats = Feats()

# Create graph
with tf.Session() as sess:
    input_ph = tf.placeholder(tf.float32, [None, NUM_FEATURES * WINDOW_SIZE])
    target_ph = tf.placeholder(tf.float32, [None, NUM_CLASSES])
    logits = inference(input_ph)
    cost = loss(logits, target_ph)
    train_oper = training(cost, INITIAL_LEARNING_RATE)
    saver = tf.train.Saver()
    summary_op = tf.merge_all_summaries()
    summary_writer = tf.train.SummaryWriter(CHECK_POINT_DIR + '/dnn_train', sess.graph)
    init = tf.initialize_all_variables()
    sess.run(init)

    if opts.mode == "t":
        # load and process training data
        ref = {}

        # Initialize reference and hypothesis dictionaries
        with open(opts.length_file, 'r') as length_file:
            for line in length_file:
                line_split = line.strip().split()
                ref[line_split[0]] = []

        # Load training labels
        with open(opts.train_labels, 'r') as ref_file:
            for line in ref_file:
                lnsplt = line.strip().split()
                ref[lnsplt[0]].append((int(lnsplt[1]), int(lnsplt[2])))
        target_sequence = []
        train_inputs = []

        for key, feat in feats:
            print('loading feats and labels for ', key)
            for label, frame in ref[key]:
                # ignore first label
                if frame > WINDOW_SIZE:
                    # convert labels to one-hot
                    if label == 0:
                        target_sequence.append([1, 0, 0, 0, 0])
                    elif label == 1:
                        target_sequence.append([0, 1, 0, 0, 0])
                    elif label == 2:
                        target_sequence.append([0, 0, 1, 0, 0])
                    elif label == 3:
                        target_sequence.append([0, 0, 0, 1, 0])
                    elif label == 4:
                        target_sequence.append([0, 0, 0, 0, 1])
                    train_inputs.append(feat[int(frame - WINDOW_SIZE / 2):int(frame + WINDOW_SIZE / 2)])

        # print(train_inputs[0], target_sequence[0])
        data = list(zip(train_inputs, target_sequence))
        # print(data[0])
        shuffle(data)
        # print(data[0])
        train_inputs, target_sequence = zip(*data)
        label_targets = np.vstack(target_sequence)
        label_targets = np.asarray(label_targets)
        print('done loading feats and labels...')

        print('running training...')
        for step in xrange(0, len(train_inputs) - BATCH_SIZE, BATCH_SIZE):
            observation = train_inputs[step:step + BATCH_SIZE]
            if np.shape(observation) == (BATCH_SIZE, WINDOW_SIZE, NUM_FEATURES):
                observation = np.reshape(observation, [BATCH_SIZE, NUM_FEATURES * WINDOW_SIZE])
                target = label_targets[step:step + BATCH_SIZE]
                target = np.reshape(target, [BATCH_SIZE, NUM_CLASSES])
                feed_dict = {input_ph: observation, target_ph: target}
                summary, _, loss_value = sess.run([summary_op, train_oper, cost], feed_dict=feed_dict)

                if step % (BATCH_SIZE * 5) == 0:
                    eval_obs = train_inputs[-(BATCH_SIZE + 50):-50]
                    eval_obs = np.reshape(eval_obs, [BATCH_SIZE, NUM_FEATURES * WINDOW_SIZE])
                    eval_target = label_targets[-(BATCH_SIZE + 50):-50]
                    eval_target = np.reshape(eval_target, [BATCH_SIZE, NUM_CLASSES])
                    eval_dict = {input_ph: eval_obs, target_ph: eval_target}

                    correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(target_ph, 1))
                    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

                    print('Step: %d, Acc: %d' % step, accuracy.eval(feed_dict=eval_dict))
                    summary_writer.add_summary(summary, step)
        save_path = saver.save(sess, "model.ckpt")

        print("Model saved in file: %s" % save_path)

    elif opts.mode == "p":
        # Restore variables from disk.
        saver.restore(sess, "model.ckpt")
        print("Model restored.")
        f = open('dnn_based_sc_v2.txt', 'w')
        hyp = {}
        # Compute hypotheses
        for key, feat in feats:
            test_inputs = []
            pred_labels = []
            frame = []
            print('loading feats for ', key)
            for i in range(0, len(feat), 10):
                if i + WINDOW_SIZE < len(feat):
                    test_inputs.append(feat[i:i + WINDOW_SIZE])
                    frame.append(i + WINDOW_SIZE / 2)
            print('done loading feats...')
            print('predicting...')
            num_examples = len(test_inputs)
            for test_step in xrange(0, len(test_inputs) - BATCH_SIZE, BATCH_SIZE):
                observation = test_inputs[test_step:test_step + BATCH_SIZE]
                observation = np.reshape(observation, [BATCH_SIZE, NUM_FEATURES * WINDOW_SIZE])
                feed_dict = {input_ph: observation}
                prediction = tf.argmax(logits, 1)
                pred = sess.run([prediction], feed_dict=feed_dict)
                # print(len(pred))
                pred_labels.extend(pred)
                # print(len(pred_labels))
            print('done predicting labels for ', key)
            # print(len(frame), len(pred_labels))
            spk_changes = np.reshape(pred_labels, (np.shape(pred_labels)[0] * np.shape(pred_labels)[1]))
            spk_changes = np.array(spk_changes)
            spk_changes = (spk_changes == 4)

            for region in regions(spk_changes):
                begin = region[0]
                end = region[1]
                change_point = int((begin + end) / 2)
                frame_to_time = frame[change_point] * 10. / 1000.
                f.write(key + '\t' + str(frame_to_time) + "\t" + str(frame_to_time) + "\tSC predicted\n")

        f.close()
    else:
        print("Mode %s not supported", opts.mode)
