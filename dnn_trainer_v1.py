#!/usr/bin/env python
import numpy as np
import optparse
import tensorflow as tf
from six.moves import xrange
from Feats import Feats

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-t", "--train_labels", dest="train_labels", default="", type="string", help="Training labels file")
optparser.add_option("-l", "--length_file", dest="length_file", default="", type="string", help="Number of frames per file")
optparser.add_option("-i", "--saved_scores", dest="saved_scores", default="", type="string", help="Previously saved model to avoid retraining")
opts = optparser.parse_args()[0]


FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_boolean('use_fp16', False,
                            """Train the model using fp16.""")

# Global constants describing the data set.
WINDOW_SIZE = 50
NUM_FEATURES = 43
NUM_CLASSES = 5
NUM_EXAMPLES_PER_EPOCH_FOR_TRAIN = 10000
NUM_EXAMPLES_PER_EPOCH_FOR_EVAL = 5000
CHECK_POINT_DIR = "/home/andus/Experiment1"

# Constants describing the training process.
MOVING_AVERAGE_DECAY = 0.9999     # The decay to use for the moving average.
NUM_EPOCHS_PER_DECAY = 350.0      # Epochs after which learning rate decays.
LEARNING_RATE_DECAY_FACTOR = 0.1  # Learning rate decay factor.
INITIAL_LEARNING_RATE = 1e-6       # Initial learning rate.
MAX_STEPS = 50000


def _variable_on_gpu(name, shape, initializer):
    """Helper to create a Variable stored on GPU memory.

    Args:
        name: name of the variable
        shape: list of ints
        initializer: initializer for Variable

    Returns:
        Variable Tensor
    """
    with tf.device('/gpu:0'):
        dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
        var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)
    return var


def _variable_with_weight_decay(name, shape, stddev, wd):
    """Helper to create an initialized Variable with weight decay.
    Note that the Variable is initialized with a truncated normal distribution.
    A weight decay is added only if one is specified.

    Args:
        name: name of the variable
        shape: list of ints
        stddev: standard deviation of a truncated Gaussian
        wd: add L2Loss weight decay multiplied by this float. If None, weight
            decay is not added for this Variable.

    Returns:
        Variable Tensor
    """
    dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
    var = _variable_on_gpu(
        name,
        shape,
        tf.truncated_normal_initializer(stddev=stddev, dtype=dtype))
    if wd is not None:
        weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var


def inference(features):
    # hidden layer 1
    with tf.variable_scope('hidden1') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        weights = _variable_with_weight_decay('weights', shape=[WINDOW_SIZE * NUM_FEATURES, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(0.1))
        hidden1 = tf.nn.relu(tf.matmul(features, weights) + biases, name=scope.name)
        # _activation_summary(hidden1)

    # hidden layer 2
    with tf.variable_scope('hidden2') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(0.1))
        hidden2 = tf.nn.relu(tf.matmul(hidden1, weights) + biases, name=scope.name)
        # _activation_summary(hidden2)

    # hidden layer 3
    with tf.variable_scope('hidden3') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(0.1))
        hidden3 = tf.nn.relu(tf.matmul(hidden2, weights) + biases, name=scope.name)
        # _activation_summary(hidden3)

    # hidden layer 4
    with tf.variable_scope('hidden4') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(0.1))
        hidden4 = tf.nn.relu(tf.matmul(hidden3, weights) + biases, name=scope.name)
        # _activation_summary(hidden4)

    # hidden layer 5
    with tf.variable_scope('hidden5') as scope:
        weights = _variable_with_weight_decay('weights', shape=[512, 512],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [512], tf.constant_initializer(0.1))
        hidden5 = tf.nn.relu(tf.matmul(hidden4, weights) + biases, name=scope.name)
        # _activation_summary(hidden5)

    # softmax, i.e. softmax(WX + b)
    with tf.variable_scope('softmax_linear') as scope:
        weights = _variable_with_weight_decay('weights', [512, NUM_CLASSES],
                                              stddev=0.04, wd=None)
        biases = _variable_on_gpu('biases', [NUM_CLASSES],
                                  tf.constant_initializer(0.0))
        softmax_linear = tf.add(tf.matmul(hidden5, weights), biases, name=scope.name)
        # _activation_summary(softmax_linear)

    return softmax_linear


def loss(logits, labels):
    """Calculates the loss from the logits and the labels.

    Args:
      logits: Logits tensor, float - [batch_size, NUM_CLASSES].
      labels: Labels tensor, int32 - [batch_size].

    Returns:
      loss: Loss tensor of type float.
    """
    # labels = tf.to_int64(labels)
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(labels * tf.log(tf.clip_by_value(logits, 1e-10, 1.0)),
                                                  reduction_indices=[1]), name='xentropy')
    # loss = tf.train.GradientDescentOptimizer(INITIAL_LEARNING_RATE).minimize(cross_entropy, name='xentropy_mean')
    return cross_entropy


def training(loss, learning_rate):
    """Sets up the training Ops.
    Creates an optimizer and applies the gradients to all trainable variables.
    The Op returned by this function is what must be passed to the
    `sess.run()` call to cause the model to train.

    Args:
      loss: Loss tensor, from loss().
      learning_rate: The learning rate to use for gradient descent.

    Returns:
      train_op: The Op for training.
    """
    # Create the gradient descent optimizer with the given learning rate.
    optimizer = tf.train.AdamOptimizer(learning_rate)
    # Create a variable to track the global step.
    global_step = tf.Variable(0, name='global_step', trainable=False)
    # Use the optimizer to apply the gradients that minimize the loss
    # (and also increment the global step counter) as a single training step.
    train_op = optimizer.minimize(loss, global_step=global_step)
    return train_op


# load and process training data
ref = {}

# Initialize reference and hypothesis dictionaries
with open(opts.length_file, 'r') as length_file:
    for line in length_file:
        line_split = line.strip().split()
        ref[line_split[0]] = []

# Load training labels
with open(opts.train_labels, 'r') as ref_file:
    for line in ref_file:
        lnsplt = line.strip().split()
        ref[lnsplt[0]].append((int(lnsplt[1]), int(lnsplt[2])))

feats = Feats()

# Create graph
g = tf.Graph()
with tf.Session(graph=g) as sess:
    input_ph = tf.placeholder(tf.float32, [None, NUM_FEATURES * WINDOW_SIZE])
    target_ph = tf.placeholder(tf.float32, [None, NUM_CLASSES])
    logits = inference(input_ph)
    cost = loss(logits, target_ph)
    train_oper = training(cost, INITIAL_LEARNING_RATE)
    # summary_op = tf.merge_all_summaries()
    init = tf.initialize_all_variables()
    saver = tf.train.Saver()
    # summary_writer = tf.train.SummaryWriter(CHECK_POINT_DIR, sess.graph)
    sess.run(init)

    if opts.saved_model == "":
        for key, feat in feats:
            target_sequence = []
            train_inputs = []
            print('loading feats for ', key)
            for label, frame in ref[key]:
                # ignore first label
                if frame > 51:
                    # convert labels to one-hot
                    if label == 0:
                        target_sequence.append([1, 0, 0, 0, 0])
                    elif label == 1:
                        target_sequence.append([0, 1, 0, 0, 0])
                    elif label == 2:
                        target_sequence.append([0, 0, 1, 0, 0])
                    elif label == 3:
                        target_sequence.append([0, 0, 0, 1, 0])
                    elif label == 4:
                        target_sequence.append([0, 0, 0, 0, 1])
                    train_inputs.append(feat[int(frame - WINDOW_SIZE / 2):int(frame + WINDOW_SIZE / 2)])

            label_targets = np.vstack(target_sequence)
            label_targets = np.asarray(label_targets)
            print('done loading features...')

            print('running training...')
            for step in xrange(len(train_inputs) - 1):
                observation = train_inputs[step]
                observation = np.reshape(observation, [1, NUM_FEATURES * WINDOW_SIZE])
                # data normalization
                # observation = (observation - np.mean(observation)) / np.std(observation)

                target = label_targets[step]
                target = np.reshape(target, [1, NUM_CLASSES])

                feed_dict = {input_ph: observation, target_ph: target}
                _, loss_value = sess.run([train_oper, cost], feed_dict=feed_dict)
                if step % 50 == 0:
                    prediction = tf.argmax(logits, 1)
                    result, pred_label = sess.run([logits, prediction], feed_dict=feed_dict)
                    print(loss_value)
                    # print('Predicted %d' % pred_label)
                    # print(target)
        save_path = saver.save(sess, "model.ckpt")
        print("Model saved in file: %s" % save_path)
    else:
        # Restore variables from disk.
        saver.restore(sess, "model.ckpt")
        print("Model restored.")

    # Compute hypotheses

