#!/usr/bin/env python
import optparse

optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Text file containing the annotations of audio file")
opts = optparser.parse_args()[0]

pre_process = []
with open(opts.input_file, 'r') as fl:
    for line in fl:
        if line != "":
            parts = line.strip().split()
            key = parts[0]

            file_id = parts[2].split('_')

            start = parts[3]
            end = parts[4]
            speaker = '_'.join(file_id[-3:-1])
            text = ' '.join(parts[6:])
            duration = float(end) - float(start)
            pre_process.append((key, float(start), text, float(duration)))

# print pre_process[0]

changes = []
last_label = pre_process[0][2]

for i in xrange(len(pre_process)):
    changes.append((pre_process[i][0], pre_process[i][1]))
    print pre_process[i][0] + '\t' + str(pre_process[i][1]) + '\t' + str(pre_process[i][1] + pre_process[i][3]) + '\t' + pre_process[i][2]
