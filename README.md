# README #

This project investigated the feasibility of using Deep Neural Networks (DNN) when applied to the Automatic Speaker Change Detection (SCD) task in the domain of recorded broadcast media. The implementation and evaluation of two approaches to the SCD task was performed in order to asses this. The first approach was a base-line system using the Bayesian Information Criteria (see *bic_based_classifier_vX.py* source files). The second approach was a system using deep neural networks trained with a novel method for extracting training examples from available datasets (see *dnn_classifier_v2.py* source file). This method made use of information of other types of changes in the audio signal apart from the speaker changes (e.g. speaker to silence, silence to speaker, etc). The results show that the DNN performs almost as well as the baseline systems, which encourages further work given that Neural Network Models are known to outperform any other model if sufficient data is fed into the model.

### What is this repository for? ###

This repository holds the development of my MSc. theses project in Artificial Intelligence during my time at The University of Edinburgh. Feedback, contributions or comments about this work will be highly appreciated.

### How do I get set up? ###

The code is writen in Python. The BIC classifiers use Python 2 and the DNN classifiers use Python 3. The code is run as a bash pipeline. 

To run the BIC classifiers and to save the result in a text file, use something like the following:

* copy-feats scp:path/to/mfccs13/feats.scp ark,t:- |  python2.7 spkr_chng/bic_based_classifier_vX.py -i 1 > eval_data_bic_based_sc_vX.txt

Note: Make sure to replace *X* with the desired version number, from 1 to 3.

To run the DNN classifier, use the following for training:

* apply-cmvn --norm-means=true --norm-vars=true scp:path/to/filterbank/cmvn.scp scp:path/to/filterbank/feats.scp ark,t:- |  python3.5 spkr_chng/dnn_classifier_v2.py -t dnn_training_labels.txt -l devel_feats.len -m t

And to detect SCPs with the trained network, use:

* apply-cmvn --norm-means=true --norm-vars=true scp:path/to/filterbank/cmvn.scp scp:path/to/filterbank/feats.scp ark,t:- |  python3.5 spkr_chng/dnn_classifier_v2.py -m p

Once the hypothesised SCPs are obtained from the classifiers, use the following to evaluate performance of the BIC based classifiers:

* python2.7 evaluation_v3.py -r eval_data_change_points_reference.txt -l path/to/mfcc13/feats.len -i eval_data_bic_based_sc_vX.txt -t 250

Note: Again, replace the *X* with the corresponding version number.

And to evaluate the DNN classifier:

* python2.7 evaluation_v3.py -r eval_data_sc_reference.txt -l path/to/filterbank/feats.len -i eval_data_dnn_based_sc_v2.txt -t 250


### Configuration ###

Before running the above scripts, you need to install Kaldi to obtain the MFCC and Filterbank features from your dataset. You also need to install Scikit Learn Tools and Google's Tensorflow to run the classifiers. Apart from all these, you also need recorded broadcast media files and the corresponding annotations. In this project, the data set used was obtained from the Multi-Genre Broadcast (MGB) Challenge (http://www.mgb-challenge.org/). A specially reliable manually annotated development set was supplied for the transcriptions and alignment tasks. This dataset, named dev.full, was used for extracting the training examples for the DNN. It consists of 48 files totaling 28.5 hours of audio. On average, there are 131 speaker changes per audio file. For testing the performance of the system, the evaluation dataset of the speech-to-text transcription task was used (evaltask1). This dataset consisted of 16 files, totaling 11 hours of audio. On average, there were a total of 330 speaker changes per file.