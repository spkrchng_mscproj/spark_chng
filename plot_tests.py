import matplotlib.pyplot as plt
import numpy as np


def xcorr():
    x, y = np.random.randn(2, 100)
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.xcorr(x, y, usevlines=True, maxlags=50, normed=True, lw=2)
    ax1.grid(True)
    ax1.axhline(0, color='black', lw=2)
    ax1.axvline(20, color='green', lw=2)

    ax2 = fig.add_subplot(212, sharex=ax1)
    ax2.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
    ax2.grid(True)
    ax2.axhline(0, color='black', lw=2)

    plt.show()


def psd():
    dt = 0.01
    t = np.arange(0, 10, dt)
    nse = np.random.randn(len(t))
    r = np.exp(-t / 0.05)

    cnse = np.convolve(nse, r) * dt
    cnse = cnse[:len(t)]
    s = 0.1 * np.sin(2 * np.pi * t) + cnse

    plt.subplot(211)
    plt.plot(t, s)
    plt.subplot(212)
    plt.psd(s, 512, 1 / dt)

    plt.show()


def stem():
    x = np.linspace(0.1, 2 * np.pi, 10)
    markerline, stemlines, baseline = plt.stem(x, np.cos(x), '-.')
    plt.setp(markerline, 'markerfacecolor', 'b')
    plt.setp(baseline, 'color', 'r', 'linewidth', 2)

    plt.show()


def ganged():
    t = np.arange(0.0, 2.0, 0.01)

    s1 = np.sin(2 * np.pi * t)
    s2 = np.exp(-t)
    s3 = s1 * s2

    # axes rect in relative 0,1 coords left, bottom, width, height.  Turn
    # off xtick labels on all but the lower plot


    f = plt.figure()
    plt.subplots_adjust(hspace=0.001)

    ax1 = plt.subplot(311)
    ax1.plot(t, s1)
    plt.yticks(np.arange(-0.9, 1.0, 0.4))
    plt.ylim(-1, 1)

    ax2 = plt.subplot(312, sharex=ax1)
    ax2.plot(t, s2)
    plt.yticks(np.arange(0.1, 1.0, 0.2))
    plt.ylim(0, 1)

    ax3 = plt.subplot(313, sharex=ax1)
    ax3.plot(t, s3)
    plt.yticks(np.arange(-0.9, 1.0, 0.4))
    plt.ylim(-1, 1)

    xticklabels = ax1.get_xticklabels() + ax2.get_xticklabels()
    plt.setp(xticklabels, visible=False)

    plt.show()


def cohere():
    # make a little extra space between the subplots
    plt.subplots_adjust(wspace=0.5)

    dt = 0.01
    t = np.arange(0, 30, dt)
    nse1 = np.random.randn(len(t))  # white noise 1
    nse2 = np.random.randn(len(t))  # white noise 2
    r = np.exp(-t / 0.05)

    cnse1 = np.convolve(nse1, r, mode='same') * dt  # colored noise 1
    cnse2 = np.convolve(nse2, r, mode='same') * dt  # colored noise 2

    # two signals with a coherent part and a random part
    s1 = 0.01 * np.sin(2 * np.pi * 10 * t) + cnse1
    s2 = 0.01 * np.sin(2 * np.pi * 10 * t) + cnse2

    plt.subplot(211)
    plt.plot(t, s1, 'b-', t, s2, 'g-')
    plt.xlim(0, 5)
    plt.xlabel('time')
    plt.ylabel('s1 and s2')
    plt.grid(True)

    plt.subplot(212)
    cxy, f = plt.cohere(s1, s2, 256, 1. / dt)
    plt.ylabel('coherence')
    plt.show()

xcorr()
# psd()
# stem()
# ganged()
# cohere()
