#!/usr/bin/env python
import numpy as np
import optparse
import re
import tensorflow as tf
from six.moves import xrange
from Feats import Feats

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Input file name containing predicted speaker changes")
optparser.add_option("-r", "--reference_file", dest="reference_file", default="", type="string", help="Reference file name containing true speaker changes")
optparser.add_option("-t", "--tolerance", dest="tolerance", default=100, type="int",
                     help="Tolerance value in milliseconds to consider speaker changes as correct")
optparser.add_option("-l", "--length_file", dest="length_file", default="", type="string", help="Number of frames per file")
opts = optparser.parse_args()[0]


FLAGS = tf.app.flags.FLAGS

# Basic model parameters.
tf.app.flags.DEFINE_integer('batch_size', 128,
                            """Number of windows to process in a batch.""")
tf.app.flags.DEFINE_boolean('use_fp16', False,
                            """Train the model using fp16.""")

# Global constants describing the data set.
WINDOW_SIZE = 101
NUM_FEATURES = 13
NUM_CLASSES = 10
NUM_EXAMPLES_PER_EPOCH_FOR_TRAIN = 10000
NUM_EXAMPLES_PER_EPOCH_FOR_EVAL = 5000
CHECK_POINT_DIR = "/home/andus/Experiment1"

# Constants describing the training process.
MOVING_AVERAGE_DECAY = 0.9999     # The decay to use for the moving average.
NUM_EPOCHS_PER_DECAY = 350.0      # Epochs after which learning rate decays.
LEARNING_RATE_DECAY_FACTOR = 0.1  # Learning rate decay factor.
INITIAL_LEARNING_RATE = 1e-6       # Initial learning rate.
MAX_STEPS = 500

# If a model is trained with multiple GPUs, prefix all Op names with tower_name
# to differentiate the operations. Note that this prefix is removed from the
# names of the summaries when visualizing a model.
TOWER_NAME = 'tower'


def _activation_summary(x):
    """Helper to create summaries for activations.

    Creates a summary that provides a histogram of activations.
    Creates a summary that measure the sparsity of activations.

    Args:
      x: Tensor
    Returns:
      nothing
    """
    # Remove 'tower_[0-9]/' from the name in case this is a multi-GPU training
    # session. This helps the clarity of presentation on tensorboard.
    tensor_name = re.sub('%s_[0-9]*/' % TOWER_NAME, '', x.op.name)
    tf.histogram_summary(tensor_name + '/activations', x)
    tf.scalar_summary(tensor_name + '/sparsity', tf.nn.zero_fraction(x))


def _variable_on_gpu(name, shape, initializer):
    """Helper to create a Variable stored on GPU memory.

    Args:
        name: name of the variable
        shape: list of ints
        initializer: initializer for Variable

    Returns:
        Variable Tensor
    """
    with tf.device('/gpu:0'):
        dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
        var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)
    return var


def _variable_with_weight_decay(name, shape, stddev, wd):
    """Helper to create an initialized Variable with weight decay.

    Note that the Variable is initialized with a truncated normal distribution.
    A weight decay is added only if one is specified.

    Args:
        name: name of the variable
        shape: list of ints
        stddev: standard deviation of a truncated Gaussian
        wd: add L2Loss weight decay multiplied by this float. If None, weight
            decay is not added for this Variable.

    Returns:
        Variable Tensor
    """
    dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
    var = _variable_on_gpu(
        name,
        shape,
        tf.truncated_normal_initializer(stddev=stddev, dtype=dtype))
    if wd is not None:
        weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var


def inference(features):
    # hidden layer 1
    with tf.variable_scope('hidden1') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        weights = _variable_with_weight_decay('weights', shape=[WINDOW_SIZE * NUM_FEATURES, 400],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [400], tf.constant_initializer(0.1))
        hidden1 = tf.nn.relu(tf.matmul(features, weights) + biases, name=scope.name)
        # _activation_summary(hidden1)

    # hidden layer 2
    with tf.variable_scope('hidden2') as scope:
        weights = _variable_with_weight_decay('weights', shape=[400, 400],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [400], tf.constant_initializer(0.1))
        hidden2 = tf.nn.relu(tf.matmul(hidden1, weights) + biases, name=scope.name)
        # _activation_summary(hidden2)

    # hidden layer 3
    with tf.variable_scope('hidden3') as scope:
        weights = _variable_with_weight_decay('weights', shape=[400, 400],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [400], tf.constant_initializer(0.1))
        hidden3 = tf.nn.relu(tf.matmul(hidden2, weights) + biases, name=scope.name)
        # _activation_summary(hidden3)

    # hidden layer 4
    with tf.variable_scope('hidden4') as scope:
        weights = _variable_with_weight_decay('weights', shape=[400, 400],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [400], tf.constant_initializer(0.1))
        hidden4 = tf.nn.relu(tf.matmul(hidden3, weights) + biases, name=scope.name)
        # _activation_summary(hidden4)

    # hidden layer 5
    with tf.variable_scope('hidden5') as scope:
        weights = _variable_with_weight_decay('weights', shape=[400, 400],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [400], tf.constant_initializer(0.1))
        hidden5 = tf.nn.relu(tf.matmul(hidden4, weights) + biases, name=scope.name)
        # _activation_summary(hidden5)

    # softmax, i.e. softmax(WX + b)
    with tf.variable_scope('softmax_linear') as scope:
        weights = _variable_with_weight_decay('weights', [400, NUM_CLASSES],
                                              stddev=0.04, wd=0.004)
        biases = _variable_on_gpu('biases', [NUM_CLASSES],
                                  tf.constant_initializer(0.0))
        softmax_linear = tf.add(tf.matmul(hidden5, weights), biases, name=scope.name)
        _activation_summary(softmax_linear)

    return softmax_linear


def loss(logits, labels):
    """Calculates the loss from the logits and the labels.

    Args:
      logits: Logits tensor, float - [batch_size, NUM_CLASSES].
      labels: Labels tensor, int32 - [batch_size].

    Returns:
      loss: Loss tensor of type float.
    """
    # labels = tf.to_int64(labels)
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(labels * tf.log(tf.clip_by_value(logits, 1e-10,1.0)),
                                                  reduction_indices=[1]), name='xentropy')
    # loss = tf.train.GradientDescentOptimizer(INITIAL_LEARNING_RATE).minimize(cross_entropy, name='xentropy_mean')
    return cross_entropy


def training(loss, learning_rate):
    """Sets up the training Ops.

    Creates a summarizer to track the loss over time in TensorBoard.

    Creates an optimizer and applies the gradients to all trainable variables.

    The Op returned by this function is what must be passed to the
    `sess.run()` call to cause the model to train.

    Args:
      loss: Loss tensor, from loss().
      learning_rate: The learning rate to use for gradient descent.

    Returns:
      train_op: The Op for training.
    """
    # Create the gradient descent optimizer with the given learning rate.
    optimizer = tf.train.AdamOptimizer(learning_rate)
    # Create a variable to track the global step.
    global_step = tf.Variable(0, name='global_step', trainable=False)
    # Use the optimizer to apply the gradients that minimize the loss
    # (and also increment the global step counter) as a single training step.
    train_op = optimizer.minimize(loss, global_step=global_step)
    return train_op


# load and process data
ref = {}

# Initialize reference and hypothesis dictionaries
with open(opts.length_file, 'r') as length_file:
    for line in length_file:
        line_split = line.strip().split()
        ref[line_split[0]] = np.zeros(int(line_split[1]), dtype=np.bool)

# Load reference change points
with open(opts.reference_file, 'r') as ref_file:
    for line in ref_file:
        lnsplt = line.strip().split()
        ref[lnsplt[0]][int(float(lnsplt[1]) * 100)] = True
        ref[lnsplt[0]][int(float(lnsplt[2]) * 100)] = True

target_sequence = []
target_indexes = []
include_noSCP_ob = True

for key in ref.keys():
    for i in xrange(len(ref[key])):
        if i + WINDOW_SIZE < len(ref[key]):
            if True in ref[key][i:i + WINDOW_SIZE]:
                index = np.where(ref[key][i:i + WINDOW_SIZE] == True)[0][0]
                target_indexes.append(i)
                # print('SCP at %d' % index)
                if index in xrange(0, 16):
                    target_sequence.append([1,0,0,0,0,0,0,0,0,0])
                    include_noSCP_ob = True
                elif index in xrange(16, 26):
                    target_sequence.append([0,1,0,0,0,0,0,0,0,0])
                elif index in xrange(26, 36):
                    target_sequence.append([0,0,1,0,0,0,0,0,0,0])
                elif index in xrange(36, 46):
                    target_sequence.append([0,0,0,1,0,0,0,0,0,0])
                elif index in xrange(46, 56):
                    target_sequence.append([0,0,0,0,1,0,0,0,0,0])
                elif index in xrange(56, 66):
                    target_sequence.append([0,0,0,0,0,1,0,0,0,0])
                elif index in xrange(66, 76):
                    target_sequence.append([0,0,0,0,0,0,1,0,0,0])
                elif index in xrange(76, 86):
                    target_sequence.append([0,0,0,0,0,0,0,1,0,0])
                elif index in xrange(86, WINDOW_SIZE):
                    target_sequence.append([0,0,0,0,0,0,0,0,1,0])
            elif include_noSCP_ob:
                # print('no SCP')
                target_sequence.append([0,0,0,0,0,0,0,0,0,1])
                target_indexes.append(i)
                include_noSCP_ob = False

label_targets = np.vstack(target_sequence)
label_targets = np.asarray(label_targets)

feats = Feats()

# Create graph
g = tf.Graph()
with tf.Session(graph=g) as sess:
    input_ph = tf.placeholder(tf.float32, [None, 1313])
    target_ph = tf.placeholder(tf.float32, [None, 10])
    logits = inference(input_ph)
    loss = loss(logits, target_ph)
    train_op = training(loss, INITIAL_LEARNING_RATE)
    # summary_op = tf.merge_all_summaries()
    init = tf.initialize_all_variables()
    # saver = tf.train.Saver()
    # summary_writer = tf.train.SummaryWriter(CHECK_POINT_DIR, sess.graph)
    sess.run(init)

    train_inputs = []
    for _, feat in feats:
        for i_feat in range(0, len(target_indexes)):
            index = target_indexes[i_feat]
            train_inputs.append(feat[index:index + WINDOW_SIZE])
    print(len(target_indexes))
    for step in xrange(MAX_STEPS):
        # print(train_inputs)
        observation = train_inputs[step]
        observation = np.reshape(observation, [1, 1313])
        observation = (observation - np.mean(observation)) / np.std(observation)
        target = label_targets[step]
        target = np.reshape(target, [1, 10])

        feed_dict = {input_ph: observation, target_ph: target}
        _, loss_value = sess.run([train_op, loss], feed_dict=feed_dict)
        if step % 50 == 0:
            prediction = tf.argmax(logits, 1)
            result, pred_label = sess.run([logits, prediction], feed_dict=feed_dict)
            print(loss_value)
            # print('Predicted %d' % pred_label)
            # print(target)
            # if False in np.equal(pred_label, np.reshape(np.asarray([0,0,0,0,0,0,0,0,0,1]), [1, 10]))

