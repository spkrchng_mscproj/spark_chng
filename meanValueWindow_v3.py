#!/usr/bin/env python
import sys
import numpy as np

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

window_with = 100
position = 0
step_size = 10
feats = []
key = ''
for (i, line) in enumerate(sys.stdin):
    if '[' in line:
        if len(feats) > 0:
            print key, position, ": ", np.mean(feats, axis=0)
        # lnsplit = line.strip().split()
        key = line.strip().split()[0]
        feats = []
        position = 0
    else:
        lnsplit = line.strip().split()
        if ']' in line:
            lnsplit = lnsplit[:-1]
        feats.append(np.array(lnsplit, dtype=np.float))
        position += 1
        if position % window_with == 0:
            print key, position, ": ", np.mean(feats, axis=0)
            feats = []

print key, np.mean(feats, axis=0)
