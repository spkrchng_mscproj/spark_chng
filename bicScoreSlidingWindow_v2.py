#!/usr/bin/env python
import numpy as np
from sklearn import mixture
import matplotlib.pyplot as plt
import optparse
from Feats import Feats

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-w", "--window_size", dest="window_size", default=200, type="int", help="Window size for analysis")
optparser.add_option("-s", "--step_size", dest="step_size", default=10, type="int", help="Step size for sliding window")
opts = optparser.parse_args()[0]


window_size = opts.window_size
step_size = opts.step_size

lmbda = 1

comps = 2
cov_type = "diag"

gL = mixture.GMM(n_components=comps, covariance_type=cov_type)
gR = mixture.GMM(n_components=comps, covariance_type=cov_type)
gAll = mixture.GMM(n_components=2 * comps, covariance_type=cov_type)

feats = Feats()

for key, feat in feats:
    # g0.fit(feat)
    wave = []
    bics = []
    frame = []
    for i in range(0, len(feat), step_size):
        if i + window_size < len(feat):
            # print key, str(i)+'-'+str((i+window_size)), np.mean(feat[i:i+window_size], axis=0)
            gAll.fit(feat[i:i + window_size])
            gL.fit(feat[i:i + window_size / 2])
            gR.fit(feat[i + window_size / 2:i + window_size])

            deltabic = np.sum(gAll.score(feat[i:i + window_size]), axis=0) - np.sum(gL.score(feat[i:i + window_size / 2]), axis=0) \
                       - np.sum(gR.score(feat[i + window_size / 2:i + window_size]), axis=0)
            bics.append(deltabic)
            frame.append(i + window_size / 2)
            print deltabic

    x1 = np.array(frame, dtype=np.float)
    y1 = np.array(bics, dtype=np.float)

    # plt.subplot(211)
    # plt.plot()
    # plt.subplot(212)
    plt.plot(x1, y1)
    plt.title('BIC Score')
    plt.xlabel('Frames')
    plt.ylabel('BIC')
    # plt.axvline(0.5)
    # plt.show()
