#!/usr/bin/env python
import numpy as np
import optparse
from sklearn.metrics import accuracy_score


optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Input file name containing predicted speaker changes")
optparser.add_option("-r", "--reference_file", dest="reference_file", default="", type="string", help="Reference file name containing true speaker changes")
optparser.add_option("-t", "--tolerance", dest="tolerance", default=100, type="int",
                     help="Tolerance value in milliseconds to consider speaker changes as correct")
optparser.add_option("-l", "--length_file", dest="length_file", default="", type="string", help="Number of frames per file")
opts = optparser.parse_args()[0]

ref = {}
hyp = {}

# Initialize reference and hypothesis dictionaries
with open(opts.length_file, 'r') as length_file:
    for line in length_file:
        line_split = line.strip().split()
        ref[line_split[0]] = np.zeros(int(line_split[1]), dtype=np.bool)
        hyp[line_split[0]] = np.zeros(int(line_split[1]), dtype=np.bool)

# Load reference change points
with open(opts.reference_file, 'r') as ref_file:
    for line in ref_file:
        lnsplt = line.strip().split()
        if int(float(lnsplt[2]) * 100) < len(ref[lnsplt[0]]):
            ref[lnsplt[0]][int(float(lnsplt[1]) * 100)] = True
            ref[lnsplt[0]][int(float(lnsplt[2]) * 100)] = True

# Load predicted change points
with open(opts.input_file, 'r') as hyp_file:
    for line in hyp_file:
        lnsplt = line.strip().split()
        hyp[lnsplt[0]][int(float(lnsplt[1]) * 100)] = True
        # hyp[lnsplt[0]][int(float(lnsplt[2]) * 100)] = True

# inputs = np.array(values, dtype=np.float)

# bool_inputs = (values < float(thres))
# print bool_inputs.shape

# Compute metrics
for key in ref.keys():
    reference = ref[key]
    hypothesis = hyp[key]
    true_positive = 0
    false_negative = 0

    comparison = np.logical_and(reference, hypothesis)
    disagree = np.logical_xor(reference, hypothesis)
    # print np.count_nonzero(comparison)
    # print np.count_nonzero(bool_inputs)
    total_changes = np.count_nonzero(reference)
    total_predicted = np.count_nonzero(hypothesis)
    print key
    print "sklearn accuracy", accuracy_score(reference, hypothesis)

    for i in xrange(opts.tolerance / 2, len(reference) - opts.tolerance / 2):
        if reference[i]:
            j = np.where(hypothesis[i - opts.tolerance / 2:i + opts.tolerance / 2] == True)[0]

            if j.size > 0:
                reference[i] = False
                hypothesis[j[0]] = False
                true_positive += 1
            else:
                false_negative += 1

    accuracy = (float(len(hypothesis) - np.count_nonzero(disagree)) / len(hypothesis))

    precision = float(true_positive) / total_predicted
    recall = float(true_positive) / total_changes

    print "Accuracy", accuracy
    print "Insertions %", float(np.count_nonzero(hypothesis)) / (total_changes + float(float(np.count_nonzero(hypothesis)))) * 100
    print "Deletions %", float(np.count_nonzero(reference)) / total_changes * 100
    print "Precision", precision
    print "Recall", recall

    if precision + recall != 0.:
        f1_score = 2. * precision * recall / (precision + recall)
        print "F1-score", f1_score
    else:
        print "F1-score N/A"
