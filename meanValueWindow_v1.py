#!/usr/bin/env python
import sys
import numpy as np

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

window_with = 100.0
feats = []
key = ''
for (i, line) in enumerate(sys.stdin):
    if '[' in line or len(feats) == window_with:
        if len(feats) > 0:
            print key, i, ": ", np.mean(feats, axis=0)
        # lnsplit = line.strip().split()
        key = line.strip().split()[0]
        feats = []
    else:
        lnsplit = line.strip().split()
        if ']' in line:
            lnsplit = lnsplit[:-1]
        feats.append(np.array(lnsplit, dtype=np.float))

print key, np.mean(feats, axis=0)
