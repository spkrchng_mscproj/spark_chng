#!/usr/bin/env python
import numpy as np
from sklearn import mixture
import matplotlib.pyplot as plt
import optparse
from Feats import Feats
from math import log

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-w", "--window_size", dest="window_size", default=200, type="int", help="Window size for analysis")
optparser.add_option("-s", "--step_size", dest="step_size", default=10, type="int", help="Step size for sliding window")
optparser.add_option("-l", "--lambda_value", dest="lambda_value", default=1, type="int", help="Penalty tuning parameter")
opts = optparser.parse_args()[0]

window_size = opts.window_size
step_size = opts.step_size
lambda_value = opts.lambda_value
dimension = 13
comps = 2

gL = mixture.GMM(n_components=comps)
gR = mixture.GMM(n_components=comps)
gAll = mixture.GMM(n_components=2 * comps)

feats = Feats()
penalty = (lambda_value / 2) * (dimension + dimension * (dimension + 1) / 2) * log(window_size)

for key, feat in feats:
    # g0.fit(feat)
    wave = []
    bics = []
    frame = []
    for i in range(0, len(feat), step_size):
        if i + window_size < len(feat):
            # print key, str(i)+'-'+str((i+window_size)), np.mean(feat[i:i+window_size], axis=0)
            gAll.fit(feat[i:i + window_size])
            gL.fit(feat[i:i + window_size / 2])
            gR.fit(feat[i + window_size / 2:i + window_size])

            L0 = np.sum(gAll.score(feat[i:i + window_size]))
            L1 = np.sum(gL.score(feat[i:i + window_size / 2])) + np.sum(gR.score(feat[i + window_size / 2:i + window_size]))

            R = L1 - L0

            deltabic = R - penalty

            bics.append(deltabic)
            frame.append(i + window_size / 2)
            print (i + window_size / 2), deltabic
        '''else:

            # print key, str(i) + '-' + str(len(feat)), np.mean(feat[i:], axis=0)
            gAll.fit(feat[i:])
            gL.fit(feat[i:i + (len(feat) - i) / 2])
            gR.fit(feat[i + (len(feat) - i) / 2:])

            deltabic = np.sum(gAll.score(feat[i:]), axis=0) - np.sum(gL.score(feat[i:(len(feat) - i) / 2]), axis=0) \
                       - np.sum(gR.score(feat[i + (len(feat) - i) / 2:]), axis=0)
            bics.append(deltabic)
            frame.append(i + (len(feat) - i) / 2)
            # print 'Delta BIC: ', deltabic, i + (len(feat) - i) / 2
        '''

    x1 = np.array(frame, dtype=np.float)
    y1 = np.array(bics, dtype=np.float)

    # plt.subplot(211)
    # plt.plot()
    # plt.subplot(212)
    plt.plot(x1, y1)
    plt.title('BIC Score')
    plt.xlabel('Frame')
    plt.ylabel('BIC')
    # plt.axvline(0.5)
    # plt.show()
    break
