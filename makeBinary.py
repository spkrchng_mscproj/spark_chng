#!/usr/bin/env python
import optparse
import struct
import numpy as np
# import matplotlib.pyplot as plt

optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Input file name containing float BIC scores as text")
optparser.add_option("-o", "--output_file", dest="output_file", default="", type="string", help="Output file name")
optparser.add_option("-n", "--in_bet_num", dest="in_between", default=10, type="int", help="Number of desired in-between points for linear regression")
opts = optparser.parse_args()[0]

values = [0.]
norm_factor = 200.0

with open(opts.input_file, 'r') as input_file:
    for i, line in enumerate(input_file):
        x_1 = i * 10. - 10.
        x_2 = i * 10.

        y_1 = values[-1]
        y_2 = float(line.strip())

        # m, b = stats.linregress([x_1, x_2], [y_1, y_2])[0:2]
        # b = stats.linregress([i - 1, i], [values[-1], new_value])[1]
        m, b = np.polyfit([x_1, x_2], [y_1, y_2], 1)[0:2]

        # print x_1
        # print "previous:", values[-1]
        for h in xrange(1, opts.in_between, 1):
            values.append((x_1 + h) * m + b)
            # print "in between:", str((x_1 + h) * m + b)
        values.append(y_2)
        # print "last:", y_2

values = np.array(values)
values /= norm_factor
values = np.clip(values, -2, 2)
# output = open(opts.output_file, 'wb')

zeros = np.zeros(90)
values = np.concatenate((zeros, values))

binary_values = struct.pack('d' * len(values), *values)

values.tofile(opts.output_file)

# output.write(binary_values)
# output.close()

'''
x_axis = [x for x in xrange(90, len(values) * 10 + 100, 10)]
plt.plot(x_axis, values)
plt.show()
'''
