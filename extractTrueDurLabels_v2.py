#!/usr/bin/env python
import optparse

optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Text file containing the annotations of audio file")
opts = optparser.parse_args()[0]

pre_process = []
with open(opts.input_file, 'r') as fl:
    for line in fl:
        parts = line.strip().split('|')

        key = parts[0]
        start = float(parts[1])
        end = float(parts[2])
        duration = end - start
        text = parts[3]

        pre_process.append((key, start, text, duration))

pre_process = sorted(pre_process, key=lambda x: x[1])
# print pre_process

changes = []

for i in xrange(len(pre_process)):
    changes.append((pre_process[i][0], pre_process[i][1]))
    print str(pre_process[i][1]) + '\t' + str(pre_process[i][1] + pre_process[i][3]) + '\t' + pre_process[i][2]
