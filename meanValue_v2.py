#!/usr/bin/env python
import sys
import numpy as np

np.set_printoptions(linewidth=np.inf)

feats = []
key = ''
for (i, line) in enumerate(sys.stdin):
    if '[' in line:
        if len(feats) > 0:
            print key, np.mean(feats, axis=0)

        lnsplit = line.strip().split()
        key = lnsplit[0]

        feats = []
    else:
        lnsplit = line.strip().split()
        if ']' in line:
            lnsplit = lnsplit[:-1]
        feats.append(np.array(lnsplit, dtype=np.float))

print key, np.mean(feats, axis=0)
