#!/usr/bin/env python
import sys
import numpy as np
from sklearn import mixture
import matplotlib.pyplot as plt

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)


# Iterable Features class
class Feats(object):
    def __init__(self, filename=None):
        if filename is None:
            self.input = sys.stdin
        else:
            self.input = open(filename, 'r')

    def get_utt(self):
        for line in self.input:
            if '[' in line:
                return line
        return False

    def __iter__(self):
        return self

    def next(self):
        feat_matrix = []
        for line in self.input:
            lnsplit = line.strip().split()
            if '[' in line:
                key = lnsplit[0]
            elif ']' in line:
                lnsplit = lnsplit[:-1]
                feat_matrix.append(lnsplit)
                return key, np.array(feat_matrix, dtype=np.float)
            else:
                feat_matrix.append(lnsplit)
        raise StopIteration


feats = Feats()

window_size = 200
step_size = 5

lmbda = 1
comps = 2
gL = mixture.GMM(n_components=comps)
gR = mixture.GMM(n_components=comps)
gAll = mixture.GMM(n_components=2 * comps)


for key, feat in feats:
    # g0.fit(feat)
    bics = []
    frame = []
    for i in range(0, len(feat), step_size):
        if i + window_size < len(feat):
            # print key, str(i)+'-'+str((i+window_size)), np.mean(feat[i:i+window_size], axis=0)
            gAll.fit(feat[i:i + window_size])
            gL.fit(feat[i:i + window_size / 2])
            gR.fit(feat[i + window_size / 2:i + window_size])

            deltabic = np.sum(gAll.score(feat[i:i + window_size]), axis=0) - np.sum(gL.score(feat[i:i + window_size / 2]), axis=0) \
                       - np.sum(gR.score(feat[i + window_size / 2:i + window_size]), axis=0)
            bics.append(deltabic)
            frame.append(i + window_size / 2)
            # print 'Delta BIC: ', deltabic, i + window_size / 2
        else:
            '''
            # print key, str(i) + '-' + str(len(feat)), np.mean(feat[i:], axis=0)
            gAll.fit(feat[i:])
            gL.fit(feat[i:i + (len(feat) - i) / 2])
            gR.fit(feat[i + (len(feat) - i) / 2:])

            deltabic = np.sum(gAll.score(feat[i:]), axis=0) - np.sum(gL.score(feat[i:(len(feat) - i) / 2]), axis=0) \
                       - np.sum(gR.score(feat[i + (len(feat) - i) / 2:]), axis=0)
            bics.append(deltabic)
            frame.append(i + (len(feat) - i) / 2)
            # print 'Delta BIC: ', deltabic, i + (len(feat) - i) / 2
            '''
            break
    x1 = np.array(frame, dtype=np.float)
    y1 = np.array(bics, dtype=np.float)
    plt.plot(x1, y1)
    plt.title('BIC Score')
    plt.xlabel('Time (s)')
    plt.ylabel('delta BIC')
    plt.show()
