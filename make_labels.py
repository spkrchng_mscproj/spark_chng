import sys
import numpy as np

NON_SPEECH_NC = 0
NON_SPEECH_TO_SPEECH = 1
SPEECH_TO_NON_SPEECH = 2
SPEECH_NC = 3
SPEECH_SC = 4

OLmaps = {}
spkrmaps = {}
convmaps = {}
spkrs = {}
lengths = {}

def regions(condition):
    d = np.diff(condition)
    idx, = d.nonzero()
    idx += 1
    if condition[0]:
        idx = np.r_[0, idx]
    if condition[-1]:
        idx = np.r_[idx, condition.size]  # Edit
    idx.shape = (-1, 2)
    return idx

with open('dev_full.convmap', 'r') as convmap:
    for line in convmap:
        lnsplt = line.strip().split()
        bbcformat = lnsplt[0]
        newformat = lnsplt[1]
        convmaps[bbcformat] = newformat
        spkrs[newformat] = []

with open(sys.argv[1], 'r') as lenfile:
    for line in lenfile:
        lnsplt = line.strip().split()
        session = lnsplt[0]
        numframes = int(lnsplt[1])
        spkrmaps[session] = []
        #OLmaps[session] = np.zeros(numframes, dtype=np.int)
        lengths[session] = numframes

with open(sys.argv[2], 'r') as stmfile:
    for line in stmfile:
        lnsplt = line.split()
        if len(lnsplt) > 5:
            session = convmaps[lnsplt[0]]
            spkr = lnsplt[2]
            numframes = lengths[session]
            if spkr not in spkrs[session]:
                spkrs[session].append(spkr)
                spkrmaps[session].append(np.zeros(numframes, dtype=np.bool))
            start = int(float(lnsplt[3]) * 100.0)
            end = int(float(lnsplt[4]) * 100.0)
            #OLmaps[convmaps[session]][start:end] += 1
            spkrnum = spkrs[session].index(spkr)
            spkrmaps[session][spkrnum][start:end] = True

labels = []

for key, values in spkrmaps.items():
    print key, len(values)
    window_size = 50
    indices = []
    for value in values:
        print value
        for region in regions(value):
            indices.append(region[0])
            indices.append((region[0] + region[1]) / 2)
            indices.append(region[1])
        for region in regions(np.logical_not(value)):
            indices.append((region[0] + region[1]) / 2)
    indices.sort()
    for i in indices:
        half_left = None
        half_right = None
        prev_max_left = window_size / 4
        prev_max_right = window_size / 4

        for speaker in range(0, len(values)):
            if prev_max_left < np.count_nonzero(values[speaker][i - window_size / 2:i]):
                half_left = speaker
                prev_max_left = np.count_nonzero(values[speaker][i - window_size / 2:i])
            if prev_max_right < np.count_nonzero(values[speaker][i:i + window_size / 2]):
                half_right = speaker
                prev_max_right = np.count_nonzero(values[speaker][i:i + window_size / 2])

        if half_left is None:
            if half_right is None:
                labels.append((key, NON_SPEECH_NC, i))
            else:
                labels.append((key, NON_SPEECH_TO_SPEECH, i))
        else:
            if half_right is None:
                labels.append((key, SPEECH_TO_NON_SPEECH, i))
            elif half_left == half_right:
                labels.append((key, SPEECH_NC, i))
            else:
                labels.append((key, SPEECH_SC, i))

f = open('dnn_training_labels.txt', 'w')
for t in labels:
    line = ' '.join(str(x) for x in t)
    f.write(line + '\n')
f.close()
