#!/usr/bin/env python
import numpy as np
from sklearn import mixture

import optparse
from Feats import Feats

# Set numpy print option:
np.set_printoptions(linewidth=np.inf)

optparser = optparse.OptionParser()
optparser.add_option("-w", "--window_size", dest="window_size", default=200, type="int", help="Window size for analysis")
optparser.add_option("-s", "--step_size", dest="step_size", default=50, type="int", help="Step size for sliding window")
optparser.add_option("-t", "--threshold", dest="threshold", default=30., type="float", help="Threshold value to consider speaker change")
optparser.add_option("-i", "--saved_scores", dest="saved_scores", default="", type="string", help="Previously saved bic scores to avoid recomputing")
optparser.add_option("-d", "--feat_dim", dest="feat_dim", default=13, type=int, help="Feature dimensionality")
opts = optparser.parse_args()[0]


window_size = opts.window_size
step_size = opts.step_size
# group_size = opts.grouping_size

dimension = opts.feat_dim
comps = 2
cov_type = "diag"

gL = mixture.GMM(n_components=comps, covariance_type=cov_type)
gR = mixture.GMM(n_components=comps, covariance_type=cov_type)
gAll = mixture.GMM(n_components=2 * comps, covariance_type=cov_type)

feats = Feats()

for key, feat in feats:
    if opts.saved_scores == "":
        # g0.fit(feat)
        wave = []
        bics = []
        frame = []
        for i in range(0, len(feat), step_size):
            if i + window_size < len(feat):
                # print key, str(i)+'-'+str((i+window_size)), np.mean(feat[i:i+window_size], axis=0)
                gAll.fit(feat[i:i + window_size])
                gL.fit(feat[i:i + window_size / 2])
                gR.fit(feat[i + window_size / 2:i + window_size])

                L0 = np.sum(gAll.score(feat[i:i + window_size]), axis=0)
                L1 = np.sum(gL.score(feat[i:i + window_size / 2]), axis=0) + np.sum(gR.score(feat[i + window_size / 2:i + window_size]), axis=0)

                deltabic = L1 - L0

                bics.append(deltabic)
                frame.append(i + window_size / 2)
                # print deltabic

        np.array(bics, dtype=np.float).tofile("scores_" + key)
        np.array(frame, dtype=np.float).tofile("scores_" + key + "_frames")
    else:
        # print "Loading previous scores."
        bics = np.fromfile("scores_" + key)
        frame = np.fromfile("scores_" + key + "_frames")
        # print "Finished loading previous scores."

    x1 = np.array(frame, dtype=np.float)
    y1 = np.array(bics, dtype=np.float)
    bool_inputs = (y1 > opts.threshold)

    # Second pass to further smooth detected changes
    prev_index = 0
    max_window = 500

    # print "Second pass estimation."
    for i, value in enumerate(bool_inputs):
        if value:
            this_index = int(x1[i])

            if this_index - prev_index > max_window:
                prev_index = this_index - max_window

            # print prev_index, this_index,
            j = np.where(bool_inputs[i + 1:] == True)[0]

            if j.size > 0:
                next_index = int(x1[i + j[0] + 1])

                if next_index - this_index > max_window:
                    next_index = this_index + max_window
            else:
                next_index = len(feat)

            gL.fit(feat[prev_index:this_index])
            gR.fit(feat[this_index:next_index])
            gAll.fit(feat[prev_index:next_index])

            L0 = np.sum(gAll.score(feat[prev_index:next_index]), axis=0)
            L1 = np.sum(gL.score(feat[prev_index:this_index]), axis=0) + np.sum(gR.score(feat[this_index:next_index]), axis=0)

            deltabic = L1 - L0
            # print deltabic

            if deltabic < opts.threshold:
                bool_inputs[i] = False
            else:
                prev_index = this_index
    # print "Finished second pass estimation."

    # Print final changes after smoothing
    # print "Saving detected sc."
    for i, value in enumerate(bool_inputs):
        if value:
            frame_to_time = x1[i] * 10. / 1000.
            print key + '\t' + str(frame_to_time) + "\t" + str(frame_to_time) + "\tSC detected"
