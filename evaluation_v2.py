#!/usr/bin/env python
import numpy as np
import optparse
import matplotlib.pyplot as plt


optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Input file name containing float BIC scores as text")
optparser.add_option("-r", "--reference_file", dest="reference_file", default="", type="string", help="Reference file name containing true SC")
optparser.add_option("-t", "--threshold", dest="threshold", default=-140., type=float, help="Threshold value to consider speaker change")
optparser.add_option("-l", "--length_file", dest="length_file", default="", type="string", help="Number of frames per file")
opts = optparser.parse_args()[0]

ref = {}
hyp = {}

# Initialize reference and hypothesis dictionaries
with open(opts.length_file, 'r') as length_file:
    for line in length_file:
        line_split = line.strip().split()
        ref[line_split[0]] = np.zeros(int(line_split[1]), dtype=np.bool)
        hyp[line_split[0]] = np.zeros(int(line_split[1]), dtype=np.bool)

# Load reference change points
with open(opts.reference_file, 'r') as ref_file:
    for line in ref_file:
        lnsplt = line.strip().split()
        ref[lnsplt[0]][int(float(lnsplt[1]) * 100)] = True
        ref[lnsplt[0]][int(float(lnsplt[2]) * 100)] = True


values = np.fromfile(opts.input_file, dtype=np.float)
'''
with open(opts.input_file, 'r') as input_file:
    for i, line in enumerate(input_file):
        values.append(float(line.strip()))
'''
# inputs = np.array(values, dtype=np.float)
accuracy = []
precision = []
recall = []
f1_score = []
for thres in xrange(-150, 0, 1):
    bool_inputs = (values < float(thres))
    # print bool_inputs.shape

    comparison = np.logical_and(ref['src-NISTRT_ses-CMU200509120900x'][0:len(bool_inputs)], bool_inputs)
    disagree = np.logical_xor(ref['src-NISTRT_ses-CMU200509120900x'][0:len(bool_inputs)], bool_inputs)
    # print np.count_nonzero(comparison)
    # print np.count_nonzero(bool_inputs)

    accuracy.append(float(len(bool_inputs) - np.count_nonzero(disagree)) / len(bool_inputs))
    precision.append(float(np.count_nonzero(comparison)) / np.count_nonzero(bool_inputs))
    recall.append(float(np.count_nonzero(comparison)) / np.count_nonzero(ref['src-NISTRT_ses-CMU200509120900x'][0:len(bool_inputs)]))
    f1_score.append(2. * precision[-1] * recall[-1] / (precision[-1] + recall[-1]))

print "Accuracy", accuracy[f1_score.index(max(f1_score))]

print "Precision", precision[f1_score.index(max(f1_score))]
print "Recall", recall[f1_score.index(max(f1_score))]
print "F1-score", max(f1_score)

plt.plot(recall, precision)
plt.title('Results')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.plot(recall[f1_score.index(max(f1_score))], precision[f1_score.index(max(f1_score))], 'o')
plt.show()
