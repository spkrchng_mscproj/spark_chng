#!/usr/bin/env python
import sys
import re

character = re.compile('(?!>\s)\d')
mean = 0.0
sum1 = 0.0
for (i, line) in enumerate(sys.stdin):
    sum1 = 0.0

    features = line.strip().split(' ')
    #print features
    for value in features:
        if character.match(value):
            sum1 += float(value)
    mean = sum1/len(features)
    if i % 10 == 0:
        print mean
        mean = 0.0
