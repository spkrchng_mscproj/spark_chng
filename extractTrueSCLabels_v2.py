#!/usr/bin/env python
import optparse

optparser = optparse.OptionParser()
optparser.add_option("-i", "--input_file", dest="input_file", default="", type="string", help="Text file containing the annotations of audio file")
opts = optparser.parse_args()[0]

pre_process = []
with open(opts.input_file, 'r') as fl:
    for line in fl:
        if line != "":
            parts = line.strip().split('|')

            file_id = parts[0].split('_')
            key = "-".join(file_id[0:6])
            start = parts[1]
            end = parts[2]
            speaker = file_id[6]
            text = parts[3]
            duration = float(end) - float(start)
            pre_process.append((key, float(start), speaker, float(duration)))

pre_process = sorted(pre_process, key=lambda x: x[1])
# print pre_process

changes = []
last_label = pre_process[0][2]

for i in xrange(1, len(pre_process)):
    if pre_process[i][2] != last_label:
        changes.append((pre_process[i][0], pre_process[i][1]))
        print pre_process[i][0] + '\t' + str(pre_process[i][1]) + '\t' + str(pre_process[i][1]) + '\t' + last_label
        last_label = pre_process[i][2]
        if pre_process[i][1] + pre_process[i][3] < pre_process[i - 1][1] + pre_process[i - 1][3]:
            last_label = pre_process[i - 1][2]
            print pre_process[i][0] + '\t' + str(pre_process[i][1] + pre_process[i][3]) + '\t' + str(pre_process[i][1] + pre_process[i][3]) + '\t' + pre_process[i][2]
